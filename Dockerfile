FROM petsymx/rails:5.2.1-ruby2.5.1-alpine

ENV RAILS_ENV='production' \
    RAKE_ENV='production' \
    RAILS_ROOT='/app/public_html'

# Set working directory
WORKDIR $RAILS_ROOT

# Adding gems
COPY Gemfile* ./
RUN bundle install --deployment --without test development --jobs 20 --retry 5
RUN gem install aws-sdk-kms aws-sdk-dynamodb

# Adding project files
COPY . .
# RUN bundle exec rake assets:precompile
RUN chmod +x $RAILS_ROOT/config/containers/*.sh

ENTRYPOINT ["/app/public_html/config/containers/boot.sh"]
CMD ["/bin/bash", "-lc", "$RAILS_ROOT/config/containers/app_cmd.sh"]
