FROM ruby:2.3.1

RUN apt-get update -qq && apt-get install -y \
    build-essential \
    nodejs \
    postgresql-client \
    bash \
    graphviz \
    nano \
    git \
    zlib1g-dev \
    musl-dev \
    libxml2-dev \
    libxslt-dev \
    tzdata \
    libyaml-dev \
    libssl-dev \
    libpq-dev

ENV LANG="C.UTF-8" \
    RAILS_ROOT="/app/public_html" \
    APP_BUNDLE_PATH="/app/vendor/bundle" \
    RAILS_ENV="development" \
    GEM_HOME="/usr/local/bundle" \
    BUNDLER_VERSION=2.0.2 \
    PATH=$GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

RUN gem uninstall bundle
RUN gem install bundler -v $BUNDLER_VERSION
RUN mkdir -p $RAILS_ROOT $APP_BUNDLE_PATH

WORKDIR $RAILS_ROOT

RUN mkdir -p config/containers/
COPY config/containers/ config/containers/
RUN chmod +x config/containers/*.sh

EXPOSE 3000

ENTRYPOINT ["/app/public_html/config/containers/boot.sh"]

CMD ["/bin/bash", "-c", "$RAILS_ROOT/config/containers/app_cmd_dev.sh"]
