# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_01_003616) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "etl_importer_admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_etl_importer_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_etl_importer_admin_users_on_reset_password_token", unique: true
  end

  create_table "etl_importer_always_discount_plan_discounts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_api_associations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_channel_rates", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "base_rate_type"
    t.string "modifier_type"
    t.boolean "turned_on"
    t.boolean "turned_off_on"
    t.decimal "rate"
    t.decimal "amount"
    t.string "channel_name"
    t.uuid "etl_importer_channel_id"
    t.uuid "etl_importer_facility_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_channel_id"], name: "index_etl_importer_channel_rates_on_etl_importer_channel_id"
    t.index ["etl_importer_facility_id"], name: "index_etl_importer_channel_rates_on_etl_importer_facility_id"
  end

  create_table "etl_importer_channels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_client_applications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.boolean "channel_rates_on"
    t.boolean "internal"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_discount_plan_api_associations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "etl_importer_discount_plan_id"
    t.uuid "etl_importer_api_association_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_api_association_id"], name: "index_etl_importer_d_plan_api_assoc_on_etl_importer_api_asso_id"
    t.index ["etl_importer_discount_plan_id"], name: "index_etl_importer_d_plan_api_assoc_on_etl_importer_d_plan_id"
  end

  create_table "etl_importer_discount_plan_client_applications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "etl_importer_discount_plan_id"
    t.uuid "etl_importer_client_application_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_client_application_id"], name: "index_etl_importer_d_pln_cli_apps_on_etl_importer_client_app_id"
    t.index ["etl_importer_discount_plan_id"], name: "index_etl_importer_d_plan_client_app_on_etl_importer_d_plan_id"
  end

  create_table "etl_importer_discount_plan_controls", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "kind"
    t.decimal "min_value"
    t.decimal "max_value"
    t.boolean "deleted"
    t.date "deleted_on"
    t.text "requirements_text"
    t.integer "applicable_discount_plans_count"
    t.uuid "etl_importer_unit_amenity_id"
    t.uuid "etl_importer_unit_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_unit_amenity_id"], name: "index_etl_importer_d_plan_cont_on_etl_importer_unit_amenity_id"
    t.index ["etl_importer_unit_type_id"], name: "index_etl_importer_d_plan_controls_on_etl_importer_unit_type_id"
  end

  create_table "etl_importer_discount_plan_discount_plan_controls", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "etl_importer_discount_plan_id"
    t.uuid "etl_importer_discount_plan_control_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_discount_plan_control_id"], name: "index_etl_importer_d_p_d_p_cont_on_etl_importer_d_plan_cont_id"
    t.index ["etl_importer_discount_plan_id"], name: "index_etl_importer_d_plan_d_plan_cont_on_etl_importer_d_plan_id"
  end

  create_table "etl_importer_discount_plan_discounts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "etl_importer_discount_plan_id"
    t.string "discount_type"
    t.integer "month_number"
    t.decimal "amount"
    t.decimal "minimum_amount"
    t.decimal "maximum_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_discount_plan_id"], name: "index_etl_importer_d_plan_discounts_on_etl_importer_d_plan_id"
  end

  create_table "etl_importer_discount_plan_facilities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "etl_importer_discount_plan_id"
    t.uuid "etl_importer_facility_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_discount_plan_id"], name: "index_etl_importer_d_plan_facilities_on_etl_importer_d_plan_id"
    t.index ["etl_importer_facility_id"], name: "index_etl_importer_d_plan_facilites_on_etl_importer_facility_id"
  end

  create_table "etl_importer_discount_plans", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.text "public_description"
    t.text "availability_text"
    t.text "requirements_text"
    t.string "kind"
    t.boolean "min_occupancy_required"
    t.boolean "min_occupancy_months"
    t.boolean "prepay_required"
    t.integer "prepay_months"
    t.date "start_date"
    t.date "end_date"
    t.boolean "auto_apply"
    t.string "promotion_type"
    t.boolean "deleted"
    t.boolean "turned_on"
    t.boolean "available_for_all_facilities"
    t.string "round_to_nearest"
    t.boolean "hide_from_website"
    t.boolean "move_in_only"
    t.boolean "existing_tenant_only"
    t.integer "priority"
    t.string "role_permission"
    t.uuid "etl_importer_always_discount_plan_discount_id"
    t.uuid "etl_importer_tenant_account_kind_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_always_discount_plan_discount_id"], name: "index_etl_importer_d_plan_on_etl_importer_always_dis_id"
    t.index ["etl_importer_tenant_account_kind_id"], name: "index_etl_importer_d_plan_on_etl_importer_tenant_acco_kind_id"
  end

  create_table "etl_importer_facilities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_invoiceable_fees", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "product_code"
    t.string "kind"
    t.string "description"
    t.boolean "required_at_move_in"
    t.boolean "required_at_transfer"
    t.decimal "amount"
    t.text "short_description"
    t.boolean "show_in_sales_center"
    t.decimal "tax_total"
    t.decimal "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_scheduled_move_outs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_tenant_account_kinds", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_unit_amenities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "short_code"
    t.boolean "show_in_sales_center_filter_dropdown"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "etl_importer_discount_plan_control_id"
    t.index ["etl_importer_discount_plan_control_id"], name: "index_etl_importer_unit_ameniti_on_etl_importer_di_plan_cont_id"
  end

  create_table "etl_importer_unit_group_discount_plans", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "etl_importer_unit_group_id"
    t.uuid "etl_importer_discount_plan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_discount_plan_id"], name: "index_etl_importer_un_gro_disc_plans_on_etl_importer_di_plan_id"
    t.index ["etl_importer_unit_group_id"], name: "index_etl_importer_un_gro_disc_plans_on_etl_importer_un_gro_id"
  end

  create_table "etl_importer_unit_group_invoiceable_fees", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "etl_importer_unit_group_id"
    t.uuid "etl_importer_invoiceable_fees_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_invoiceable_fees_id"], name: "index_etl_importer_un_gpo_inv_fees_on_etl_importer_inv_fees_id"
    t.index ["etl_importer_unit_group_id"], name: "index_etl_importer_un_gpo_invo_fees_on_etl_importer_un_gpo_id"
  end

  create_table "etl_importer_unit_group_unit_amenities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "etl_importer_unit_amenities_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "etl_importer_unit_group_id"
    t.index ["etl_importer_unit_amenities_id"], name: "index_etl_importer_unit_gop_un_amen_on_etl_importer_un_amen_id"
    t.index ["etl_importer_unit_group_id"], name: "index_etl_importer_uni_group_un_amen_on_etl_importer_un_gop_id"
  end

  create_table "etl_importer_unit_groups", id: :string, force: :cascade do |t|
    t.decimal "price"
    t.string "group_key"
    t.string "name"
    t.integer "available_units_count"
    t.integer "total_units_count"
    t.integer "total_non_excluded_units_count"
    t.string "size"
    t.integer "standard_rate"
    t.integer "floor"
    t.decimal "reduced_price"
    t.integer "occupancy_percent"
    t.integer "area"
    t.integer "length"
    t.integer "width"
    t.integer "height"
    t.integer "due_at_move_in"
    t.integer "due_at_move_in_without_fees"
    t.integer "due_monthly"
    t.text "attribute_description"
    t.text "description"
    t.integer "average_rent"
    t.string "active_rate_type"
    t.uuid "etl_importer_channel_rate_id"
    t.uuid "etl_importer_unit_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_channel_rate_id"], name: "index_etl_importer_unit_groups_on_etl_importer_channel_rate_id"
    t.index ["etl_importer_unit_type_id"], name: "index_etl_importer_unit_groups_on_etl_importer_unit_type_id"
  end

  create_table "etl_importer_unit_types", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.boolean "deleted"
    t.string "internal_account_code"
    t.text "code_and_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etl_importer_units", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.decimal "price"
    t.string "name"
    t.text "description"
    t.integer "width"
    t.integer "length"
    t.integer "height"
    t.integer "door_height"
    t.integer "door_width"
    t.string "door_type"
    t.string "access_type"
    t.string "floor"
    t.string "size"
    t.integer "area"
    t.integer "standard_rate"
    t.string "managed_rate"
    t.boolean "available_for_move_in"
    t.boolean "rentable"
    t.string "status"
    t.string "payment_status"
    t.string "current_ledger_id"
    t.string "current_tenant_id"
    t.string "combination_lock_number"
    t.string "attribute_description"
    t.boolean "deleted"
    t.boolean "damaged"
    t.boolean "complimentary"
    t.uuid "etl_importer_channel_rate_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["etl_importer_channel_rate_id"], name: "index_etl_importer_units_on_etl_importer_channel_rate_id"
  end

  create_table "units", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price", precision: 8, scale: 2
    t.string "name"
    t.text "description"
    t.float "area"
    t.string "uom"
  end

  add_foreign_key "etl_importer_channel_rates", "etl_importer_channels"
  add_foreign_key "etl_importer_channel_rates", "etl_importer_facilities"
  add_foreign_key "etl_importer_discount_plan_api_associations", "etl_importer_api_associations"
  add_foreign_key "etl_importer_discount_plan_api_associations", "etl_importer_discount_plans"
  add_foreign_key "etl_importer_discount_plan_client_applications", "etl_importer_client_applications"
  add_foreign_key "etl_importer_discount_plan_client_applications", "etl_importer_discount_plans"
  add_foreign_key "etl_importer_discount_plan_controls", "etl_importer_unit_amenities"
  add_foreign_key "etl_importer_discount_plan_controls", "etl_importer_unit_types"
  add_foreign_key "etl_importer_discount_plan_discount_plan_controls", "etl_importer_discount_plan_controls"
  add_foreign_key "etl_importer_discount_plan_discount_plan_controls", "etl_importer_discount_plans"
  add_foreign_key "etl_importer_discount_plan_discounts", "etl_importer_discount_plans"
  add_foreign_key "etl_importer_discount_plan_facilities", "etl_importer_discount_plans"
  add_foreign_key "etl_importer_discount_plan_facilities", "etl_importer_facilities"
  add_foreign_key "etl_importer_discount_plans", "etl_importer_always_discount_plan_discounts"
  add_foreign_key "etl_importer_discount_plans", "etl_importer_tenant_account_kinds"
  add_foreign_key "etl_importer_unit_amenities", "etl_importer_discount_plan_controls"
  add_foreign_key "etl_importer_unit_group_discount_plans", "etl_importer_discount_plans"
  add_foreign_key "etl_importer_unit_group_discount_plans", "etl_importer_unit_groups"
  add_foreign_key "etl_importer_unit_group_invoiceable_fees", "etl_importer_invoiceable_fees", column: "etl_importer_invoiceable_fees_id"
  add_foreign_key "etl_importer_unit_group_invoiceable_fees", "etl_importer_unit_groups"
  add_foreign_key "etl_importer_unit_group_unit_amenities", "etl_importer_unit_amenities", column: "etl_importer_unit_amenities_id"
  add_foreign_key "etl_importer_unit_group_unit_amenities", "etl_importer_unit_groups"
  add_foreign_key "etl_importer_unit_groups", "etl_importer_channel_rates"
  add_foreign_key "etl_importer_unit_groups", "etl_importer_unit_types"
  add_foreign_key "etl_importer_units", "etl_importer_channel_rates"
end
