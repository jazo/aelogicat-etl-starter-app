#!/bin/sh

set -eu

unset BUNDLE_PATH
unset BUNDLE_BIN

exec $@
