#!/bin/bash

echo '----Start Aplication----'
bundle exec rails db:exists && bundle exec rails db:migrate || bundle exec rails db:setup
bundle exec rails assets:precompile
exec bundle exec puma -C config/puma.rb
