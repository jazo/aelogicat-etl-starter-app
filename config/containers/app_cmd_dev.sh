#!/bin/bash

echo '------------------------'
echo '----Start Aplication----'
echo '------------------------'
# gem install bundler -v 2.0.2
bundler install -j3 --path=$APP_BUNDLE_PATH
bundler update -j3
bundle exec rails db:exists && bundle exec rails db:migrate || bundle exec rails db:setup
exec bundle exec puma -C config/puma.rb
