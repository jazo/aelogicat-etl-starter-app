ETL Starter Application
=======================

Rails application with just enough code to start AELOGICA's [ETL exercise](https://git.appexpress.io/open-source/etl-exercise).

### Prerequisites

- at least Ruby 2.3.1

### Instructions

1. Clone from https://git.appexpress.io/open-source/etl-starter-app.git
1. Install gems
1. Run application


### To run inside Docker

#### Start dokcer with
```
docker network create aelogica-host-network
docker-compose up -d --build
```

#### Add etl_starter_app.local to your hostfile

##### On windows
- open an elevated promp
- open a the host file (C:\Windows\System32\drivers\etc\hosts) and add the host pointing to your localhost
```
127.0.0.1  etl_starter_app.local
```

#### Access container
```
docker-compose exec app bash
```

#### Enter rails console 
```
docker-compose exec app bundle rails c
```
