# ## Schema Information
#
# Table name: `units`
#
# ### Columns
#
# Name               | Type               | Attributes
# ------------------ | ------------------ | ---------------------------
# **`area`**         | `float`            |
# **`created_at`**   | `datetime`         | `not null`
# **`description`**  | `text`             |
# **`id`**           | `bigint(8)`        | `not null, primary key`
# **`name`**         | `string`           |
# **`price`**        | `decimal(8, 2)`    |
# **`uom`**          | `string`           |
# **`updated_at`**   | `datetime`         | `not null`
#

class Unit < ApplicationRecord
end
